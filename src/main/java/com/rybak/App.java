package com.rybak;

import com.rybak.model.exceptions.MyAutoClosable;
import com.rybak.view.CompanyMenu;

public class App {


    public static void main(String[] args) throws Exception {
        new CompanyMenu().start();

        try (MyAutoClosable myAutoClosable = new MyAutoClosable()){

        }
    }
}
