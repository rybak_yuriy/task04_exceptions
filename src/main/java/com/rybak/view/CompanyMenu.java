package com.rybak.view;

import com.rybak.controller.CompanyController;
import com.rybak.model.Department;
import com.rybak.model.Employee;
import com.rybak.model.exceptions.EmployeeNotFoundException;
import com.rybak.model.exceptions.WrongTypeException;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CompanyMenu {

    private static Scanner INPUT = new Scanner(System.in);

    private CompanyController companyController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public CompanyMenu() {
        companyController = new CompanyController();

        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - show all employees in company");
        menu.put("2", " 2 - add new employee to company");
        menu.put("3", " 3 - show all employees from department");
        menu.put("4", " 4 - find employee by name");
        menu.put("5", " 5 - remove employee by name");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }


    private void pressButton1() {
        System.out.println("All employees: ");
        List<Employee> employees = companyController.showAllEmployees();
        employees.forEach(System.out::println);

    }

    private void pressButton2() throws WrongTypeException {
        System.out.print("-> Please enter a name of employee: ");
        String name = null;
        try {
            name = INPUT.next();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

        System.out.println("-> Please choose a department: ");
        for (int i = 0; i < Department.values().length; i++) {
            System.out.println(i + "." + Department.values()[i]);
        }
        int num = 0;
        try {
            num = INPUT.nextInt();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        Department department = Department.values()[num];

        System.out.print("-> Please enter an age: ");
        int age = 0;
        try {
            age = INPUT.nextInt();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

        companyController.addEmployee(name, department, age);

    }

    private void pressButton3() {
        System.out.println("-> Please choose a department: ");
        for (int i = 0; i < Department.values().length; i++) {
            System.out.println(i + "." + Department.values()[i]);
        }
        int num = INPUT.nextInt();
        Department department = Department.values()[num];
        List<Employee> list = companyController.showEmployeesFromDepartment(department);
        list.forEach(System.out::println);

    }

    private void pressButton4() throws EmployeeNotFoundException {
        System.out.print("-> Please enter a name of employee: ");
        String name = INPUT.next();
        try {
            Employee employee = companyController.findEmployeeByName(name);
            if (employee != null) {
                System.out.println("Employee with name " + name + " found");
                System.out.println(employee);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void pressButton5() throws EmployeeNotFoundException {
        System.out.print("-> Please enter a name of employee whom you want to remove: ");
        String name = INPUT.next();
        try {
            companyController.removeEmployeeByName(name);
            System.out.print("Employee with name " + name + " is removed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = INPUT.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

}



