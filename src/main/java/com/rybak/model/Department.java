package com.rybak.model;

public enum Department {

    SALES_DEPARTMENT,
    FINANCIAL_DEPARTMENT,
    HR_DEPARTMENT,
    TECHNICAL_DEPARTMENT

}
