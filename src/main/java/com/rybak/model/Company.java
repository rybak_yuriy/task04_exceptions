package com.rybak.model;

import com.rybak.model.exceptions.EmployeeNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Company {


    private String companyName;
    private List<Employee> employees;

    public Company(String companyName) {
        this.companyName = companyName;
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public Employee findEmployeeByName(String name) {
        return employees.stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with name " + name + " not found"));
    }

    public void removeEmployee(Employee employee) {
        if (employees.contains(employee)) {
            employees.remove(employee);
        } else throw new EmployeeNotFoundException("Employee with name " + employee.getName() + " not found");

    }

    public List<Employee> showAllEmployees() {
        return employees;
    }

    public List<Employee> showEmployeesFromDepartment(Department department) {
        return employees.stream()
                .filter(e -> (e.getDepartment().equals(department)))
                .collect(Collectors.toList());

    }

    @Override
    public String toString() {
        return "Company{" +
                ", employees=" + employees +
                '}';
    }
}
