package com.rybak.model.exceptions;

public class MyAutoClosable implements AutoCloseable{
    @Override
    public void close() throws Exception {
        System.out.println("MyAutoClosable works");
    }
}
