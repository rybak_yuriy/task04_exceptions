package com.rybak.model.exceptions;

import java.util.InputMismatchException;

public class WrongTypeException extends InputMismatchException {
    public WrongTypeException() {
    }

    public WrongTypeException(String s) {
        super(s);
    }


}
