package com.rybak.model;

public class Employee {

    private String name;
    private Department department;
    private int age;

    public Employee(String name, Department department, int age) {
        this.name = name;
        this.department = department;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name= '" + name + '\'' +
                ", department= " + department +
                ", age= " + age +
                '}';
    }
}
