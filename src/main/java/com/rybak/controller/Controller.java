package com.rybak.controller;

import com.rybak.model.Department;
import com.rybak.model.Employee;

import java.util.List;

public interface Controller {


    public void addEmployee(String name, Department department, int age);
    List<Employee> showAllEmployees();
    public void removeEmployeeByName(String name);
    public List<Employee> showEmployeesFromDepartment(Department department);
    public Employee findEmployeeByName(String name);


}
