package com.rybak.controller;

import com.rybak.model.Company;
import com.rybak.model.Department;
import com.rybak.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class CompanyController implements Controller {

    Company company;

    public CompanyController() {
        company = new Company("My Company");
    }

    @Override
    public void addEmployee(String name, Department department, int age) {
        company.addEmployee(new Employee(name,department,age));
    }

    @Override
    public List<Employee> showAllEmployees() {
        return company.showAllEmployees();
    }

    @Override
    public void removeEmployeeByName(String name) {
        Employee employee=company.findEmployeeByName(name);
        company.removeEmployee(employee);

    }

    @Override
    public List<Employee> showEmployeesFromDepartment(Department department) {

       return company.showEmployeesFromDepartment(department);
    }

    @Override
    public Employee findEmployeeByName(String name) {
        return company.findEmployeeByName(name);
    }
}
